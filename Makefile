# -*- makefile -*-

# deployment
RUN_AS_USER = yrik
PROCESSES   = 4

# definitions
APP           = golinko
HOST          = 127.0.0.1
PORT          = 8877
PID 	      = $(shell pwd)/django.pid
CURRPATH      = $(shell pwd)
SPHINXBUILD   = sphinx-build
BUILDDIR      = docs/_build


PROJECT_TEST_TARGETS=landingpages sitepages sidebars

# constants
PYTHONPATH = .:..
PYTHON     = python

MANAGE=python manage.py

# end

run:
	$(MAKE) clean
	$(MANAGE) runserver $(HOST):$(PORT)

syncdb:
	$(MAKE) clean
	$(MANAGE) syncdb --noinput
	$(MAKE) manage -e CMD="migrate"

fresh_syncdb:
	-rm ./db.sqlite
	$(MANAGE) syncdb --noinput
	$(MAKE) manage -e CMD="migrate"
	@echo Loading initial fixtures...
	$(MANAGE) loaddata $(APP)/base_initial_data.json
	@echo Done

test:
	$(MAKE) clean
	TESTING=1 $(MANAGE) test $(TEST_OPTIONS) $(PROJECT_TEST_TARGETS)

clean:
	@echo Cleaning up *.pyc files
	find . | grep '.pyc$$' | xargs -I {} rm {}

migrate:
ifndef APP_NAME
	@echo Please, specify -e APP_NAME=appname argument
else
	@echo Starting of migration of $(APP_NAME)
	-$(MANAGE) schemamigration $(APP_NAME) --auto
	$(MANAGE) migrate $(APP_NAME)
	@echo Done
endif

init_migrate:
ifndef APP_NAME
	@echo Please, specify -e APP_NAME=appname argument
else
	@echo Starting init migration of $(APP_NAME)
	$(MANAGE) schemamigration $(APP_NAME) --initial
	$(MANAGE) migrate $(APP_NAME)
	@echo Done
endif

shell:
	$(MAKE) clean
	$(MANAGE) shell

manage:
ifndef CMD
	@echo Please, specify CMD argument to execute Django management command
else
	$(MANAGE)  $(CMD)
endif

help:
	@cat README

# production activation/deactivation
uwsgi_reload:
	-$(MAKE) uwsgi_stop
	$(MAKE) uwsgi

uwsgi:
	$(MANAGE) runfcgi method=threaded host=$(HOST) port=$(PORT) pidfile=$(PID)
	@echo "uwsgi on $(CURRPATH):$(PORT) started"

uwsgi_stop:
	-kill -9 $(shell cat $(PID))
	-rm -f $(PID)
	 
	@echo "uwsgi on $(CURRPATH):$(PORT) stopped"
