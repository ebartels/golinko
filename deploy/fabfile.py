import os
from datetime import datetime
from fabric.api import run, sudo, env, local, cd, task, require
from fabric.colors import red, green


# Env
env.user = 'golinko'
env.shell = "/bin/bash -c"
env.hosts = ['107.20.137.254']
env.database_name = 'golinko'
env.forward_agent = True
env.site_path = '/srv/www/sites/golinkodesign/'
env.git_remote = 'origin'
env.site_url = 'http://www.golinkodesign.com/'
env.vagrant = False
env.vagrant_user = 'vagrant'
env.vagrant_ssh_key = '~/.ssh/id_rsa'

# Local Paths
env.local_path = os.path.dirname(__file__)

# Chef
#env.chef_config = env.local_path
#env.chef_path = '/etc/chef/'
#env.chef_executable = '/var/lib/gems/1.8/bin/chef-solo'


# Helper Tasks
@task(alias='va')
def vagrant():
    """
    Runs all further commands on the local vagrant vm.
    usage: fab vagrant deploy
    """
    env.vagrant = True
    env.hosts = ['%s@127.0.0.1:2222' % env.vagrant_user]
    vagrant_ssh_key = getattr(env, 'vagrant_ssh_key', None)
    if vagrant_ssh_key:
        env.key_filename = os.path.expanduser(vagrant_ssh_key)
    else:
        result = local('vagrant ssh_config | grep IdentityFile', capture=True)
        env.key_filename = result.split()[1]


@task(alias='venv')
def virtualenv(command, cwd=None):
    """
    Run the command inside a virtualenv.
    """
    require('site_path')
    activate = os.path.join(env.site_path, 'bin', 'activate')
    with cd(cwd or env.site_path):
        run('source ' + activate + ' && ' + command)


@task
def djcommand(command):
    """
    Shortcut for running django commands
    """
    require('site_path')
    path = os.path.join(env.site_path, 'django')
    virtualenv("PYTHONPATH=%s python manage.py %s" % (path, command),
                                                          cwd=path)


@task
def rsync(local_path, remote_path, sdo=False, exclude=None):
    """
    Uses rsync to push to remote hosts.

    sdo: run sync with sudo

    NOTE: we always add the trailing '/' to local_path, which will means we are
    copying the contents of the directory rather than the directory itself.
    """
    if not local_path.endswith('/'):
        local_path = local_path + '/'

    for host in env.hosts:
        # figure out remote host/user/path
        if ':' in host:
            host, port = host.split(':')
        else:
            port = None
        remote_uri = "%s:%s" % (host, remote_path)
        if '@' not in host:
            remote_uri = "%s@%s" % (env.user, remote_uri)

        # Figure out ssh options
        if port:
            exc = "ssh -q -p %s" % port
        else:
            exc = "ssh -q"

        # Use ssh keyfile
        key_filename = getattr(env, 'key_filename', None)
        if key_filename:
            exc = "%s -i %s" % (exc, key_filename)

        # Run with sudo
        cmd = 'rsync --delete-after --exclude=*.swp --exclude=*.swo'
        if exclude:
            cmd = '%s %s' % (cmd,
                             ' '.join(['--exclude=%s' % e for e in exclude]))
        if sdo:
            # This is to pre-activate sudo permissions
            local("stty -echo; ssh %s sudo -v; stty echo" % host)
            cmd = '%s --rsync-path="sudo rsync"' % cmd

        local('%s -e "%s" -av %s  %s' % (cmd, exc, local_path, remote_uri))


@task
def sdorsync(local_path, remote_path, exclude=None):
    rsync(local_path, remote_path, sdo=True, exclude=exclude)


# Server Tasks
@task
def uname():
    run('uname -a')


@task
def restart():
    """
    Reloads the application server.
    """
    sudo('restart golinko')


@task
def reload():
    "Reload the server."
    sudo('reload golinko')


@task
def reload_memcached():
    "Reloads the memcached daemon"
    sudo('service memcached restart', pty=False)


@task
def check():
    require('site_url')
    response = local('curl --silent -I "%s"' % env.site_url, capture=True)
    if '200 OK' in response:
        print(green('\nLooks good from here!\n'))
        return True
    else:
        print(red('\nSOMETHING WENT HORRIBLY WRONG!'))
        return False


def tail(logfile, follow=False, sdo=False):
    if sdo:
        run_ = sudo
    else:
        run_ = run
    if follow:
        run_('tail -f %s' % logfile)
    else:
        run_('tail %s' % logfile)


@task(alias='tailgun')
def tailgunicorn(follow=False):
    """Tail the Gunicorn log file."""
    require('site_path')
    tail(os.path.join(env.site_path, 'log/gunicorn.log'), follow)


@task(alias='tailgerror')
def tailgunicornerror(follow=False):
    """Tail the Gunicorn log file."""
    require('site_path')
    tail(os.path.join(env.site_path, 'log/gunicorn.error.log'), follow)


@task(alias='taillog')
def tailaccesslog(follow=False):
    tail('/var/log/nginx/golinkodesign.access.log', follow, sdo=True)


@task(alias='tailerr')
def tailerrorlog(follow=False):
    tail('/var/log/nginx/error.log', follow, sdo=True)


# Deployment tasks
@task
def deploy(syncdb=False):
    """
    Updates code , and reloads the server, optionally runs syncdb.
    """
    require('site_path', 'database_name')

    # Run git checkout
    djpath = os.path.join(env.site_path, 'django')
    with cd(djpath):
        run('git pull origin master')

    # Sync database
    if syncdb:
        with cd(env.site_path):
            d = datetime.now().strftime('%Y%m%d-%H%M%S')
            run("pg_dump %s | gzip > '%s.sql.gz'" % (env.database_name, d))
        djcommand("syncdb --noinput --migrate")

    # collect static media
    djcommand('collectstatic --noinput')

    # Reload server
    reload()
    reload_memcached()

    # all done
    with cd(djpath):
        run('git log -n1')


# TODO: test out these chef provisioning tasks
#@task
#def install_chef():
#    sudo('apt-get update')
#    sudo('apt-get install -y rubygems ruby ruby-dev')
#    sudo('gem install chef --no-ri --no-rdoc')
#
#
#@task
#def sync_chef():
#    """
#    Sync chef config to remote host
#    """
#    require('chef_config')
#
#    # Sync diretories
#    remote_path = "/etc/chef/"
#    sudo('mkdir -p %s' % remote_path)
#    sdorsync(env.chef_config, remote_path, exclude=(
#        'Vagrantfile', 'fabfile.p*', '.vagrant'))
#    sudo('chown -R root:admin %s' % remote_path)
#    sudo('chmod 770 %s' % remote_path)
#    sudo('find %s -type d -exec chmod 770 {} \\;' % remote_path)
#    sudo('find %s -type f -exec chmod 660 {} \\;' % remote_path)
#
#
#@task
#def run_chef():
#    require('chef_executable', 'chef_path')
#    sync_chef()
#    sudo('cd %s && %s' % (env.chef_path, env.chef_executable))
