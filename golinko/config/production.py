"""
Settings for use in production.
"""
from .defaults import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG

MANAGERS = list(MANAGERS) + [
    #('', ''),
]

# Cache Setup
CACHE_TIMEOUT = 60 * 10
CACHE_PREFIX = 'GOLINKO'
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': '127.0.0.1:11211',
        'KEY_PREFIX': CACHE_PREFIX,
        'TIMEOUT': CACHE_TIMEOUT
    }
}
SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"
#SESSION_ENGINE = "django.contrib.sessions.backends.db"
CACHE_MIDDLEWARE_SECONDS = CACHE_TIMEOUT
CACHE_MIDDLEWARE_KEY_PREFIX = CACHE_PREFIX
CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True

# Cached template loader
TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        TEMPLATE_LOADERS
    )),
)

# File Storage
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
MEDIA_URL = 'https://golinko-media.s3.amazonaws.com/'

# AWS Storage Settings
AWS_STORAGE_BUCKET_NAME = 'golinko-media'
AWS_QUERYSTRING_AUTH = False
AWS_S3_SECURE_URLS = False
AWS_HEADERS = {
    'Cache-Control': 'max-age=864000',
}

# Use Amazon SES for sending mails
#EMAIL_BACKEND = 'django_ses.SESBackend'

# Temp folders
TEMP_DIR = '/var/tmp/golinko/'
FILE_UPLOAD_TEMP_DIR = os.path.join(TEMP_DIR, 'uploads')

LOG_DIR = os.path.join(os.path.dirname(BASE_PATH), '..', 'log')
if os.path.exists(LOG_DIR):
    LOGGING['handlers'].update({
        'logfile': {
            'level': 'DEBUG',
            'formatter': 'regular',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOG_DIR, 'django.log'),
            'maxBytes': '16777216', # 16megabytes
        },
    })
    LOGGING['loggers'].update({
        'cms': {
            'handlers': ['logfile'],
            'level': 'WARN',
            'propagate': True,
        },
    })

STATIC_ROOT = os.path.join(os.path.dirname(BASE_PATH), '..', 'htdocs', 'static')

# Django compressor
COMPRESS_ROOT = STATIC_ROOT
COMPRESS_ENABLED = True
COMPRESS_PRECOMPILERS = (
    # LESS files (note: make sure lessc is installed and on the your PATH)
    ('text/less', 'lessc {infile} {outfile}'),
)
