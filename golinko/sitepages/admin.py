from django.contrib import admin
from django.db import models
from .models import SitePage, DesignOfficePage,\
    SelectedClientsPage, CeePage, ContactPage, EssentialsPage, Category
from .forms import DesignOfficePageForm, SitePageForm, \
    SelectedClientsPageForm, CeePageForm, ContactPageForm, EssentialsPageForm

from cms.apps.utils.forms.widgets import VisualEditor
from cms.apps.media.admin.options import RelatedImagesInline
from cms.apps.media.admin.widgets import ImageForeignKeyWidget
from cms.apps.media.models import Image

from golinko.sidebars.admin import CustomSortableCKAdmin


class SitePageAdmin(admin.ModelAdmin):
    form = SitePageForm
    list_display = ('title', 'active',)
    prepopulated_fields = {'slug': ('title',)}
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'headline', 'meta_keywords',
                       'meta_description', 'category', 'active')
        }),
        ('Main Content', {
            'fields': ('content1_title', 'content1_text', 'content1_image',
                       'content2_title', 'content2_text', 'link_text')
        }),
        ('Second Row', {
            'fields': ('content3_title', 'content3_text', 'content3_image',
                       'show_contact_form',
                       'content4_title', 'content4_text')
        }),
        ('Site Map', {
            'fields': ('show_in_sitemap', 'sitemap_title',)
        }),
    )
    raw_id_fields = ('content3_image',)
    inlines = [RelatedImagesInline]

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'content3_image':
            kwargs.pop('request')
            kwargs['widget'] = ImageForeignKeyWidget(
                db_field.rel, admin_site=self.admin_site)
            return db_field.formfield(**kwargs)
        elif db_field.name == 'content1_image':
            kwargs.pop('request')
            kwargs['widget'] = ImageForeignKeyWidget(
                db_field.rel, admin_site=self.admin_site)
            return db_field.formfield(**kwargs)
        elif (isinstance(db_field, models.TextField) and
                not db_field.name.startswith('meta')):
            kwargs['widget'] = VisualEditor
        return super(SitePageAdmin, self).formfield_for_dbfield(
            db_field, **kwargs)


class DesignOfficePageAdmin(admin.ModelAdmin):
    form = DesignOfficePageForm
    list_display = ('title', 'active',)
    prepopulated_fields = {'slug': ('title',)}
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'headline', 'meta_keywords',
                       'meta_description', 'category', 'active')
        }),
        ('Icons', {
            'fields': ('menu_image_default', 'menu_image_hover',
                       'menu_image_active')
        }),
        ('Main Content', {
            'fields': ('content1_title', 'content1_text', 'content1_image',
                       'content2_title', 'content2_text')
        }),
        ('Second Row', {
            'fields': ('content3_title', 'content3_text', 'content3_image',
                       'show_contact_form',
                       'content4_title', 'content4_text')
        }),
        ('Site Map', {
            'fields': ('show_in_sitemap', 'sitemap_title',)
        }),)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if 'image' in db_field.name:
            kwargs.pop('request')
            kwargs['widget'] = ImageForeignKeyWidget(
                db_field.rel, admin_site=self.admin_site)
            return db_field.formfield(**kwargs)
        elif (isinstance(db_field, models.TextField) and
              not db_field.name.startswith('meta')):
            kwargs['widget'] = VisualEditor
        return super(DesignOfficePageAdmin, self).formfield_for_dbfield(
            db_field, **kwargs)


class SelectedClientsPageAdmin(DesignOfficePageAdmin):
    form = SelectedClientsPageForm
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'headline', 'meta_keywords',
                       'meta_description', 'category', 'active')
        }),
        ('Icons', {
            'fields': ('menu_image_default', 'menu_image_hover',
                       'menu_image_active')
        }),
        ('Main Content', {
            'fields': ('content1_title', 'content1_text', 'content1_image',
                       'col1_text', 'col2_text')
        }),
        ('Second Row', {
            'fields': ('content3_title', 'content3_text', 'content3_image',
                       'show_contact_form',
                       'content4_title', 'content4_text')
        }),
        ('Site Map', {
            'fields': ('show_in_sitemap', 'sitemap_title',)
        }),)


class EssentialsPageAdmin(DesignOfficePageAdmin):
    form = EssentialsPageForm
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'headline', 'meta_keywords',
                       'meta_description', 'category', 'active')
        }),
        ('Icons', {
            'fields': ('menu_image_default', 'menu_image_hover',
                       'menu_image_active')
        }),
        ('Main Content', {
            'fields': ('content1_title', 'essential1_title', 'essential1_text',
                       'essential2_title', 'essential2_text',
                       'essential3_title', 'essential3_text',
                       'essential4_title', 'essential4_text',
                       'essential5_title', 'essential5_text',
                       'essential6_title', 'essential6_text',
                       'essential7_title', 'essential7_text',
                       'essential8_title', 'essential8_text',
                       'essential9_title', 'essential9_text',
                       'essential10_title', 'essential10_text',
                       'essential11_title', 'essential11_text',
                       'essential12_title', 'essential12_text',)
        }),
        ('Second Row', {
            'fields': ('content3_title', 'content3_text', 'content3_image',
                       'show_contact_form',
                       'content4_title', 'content4_text')
        }),
        ('Site Map', {
            'fields': ('show_in_sitemap', 'sitemap_title',)
        }),)


class CeePageAdmin(DesignOfficePageAdmin):
    form = CeePageForm
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'headline', 'meta_keywords',
                       'meta_description', 'category', 'active')
        }),
        ('Icons', {
            'fields': ('menu_image_default', 'menu_image_hover',
                       'menu_image_active')
        }),
        ('Main Content', {
            'fields': ('content1_title', 'content1_text', 'content1_image')
        }),
        ('Second Row', {
            'fields': ('content3_title', 'content3_text', 'content3_image',
                       'show_contact_form',
                       'content4_title', 'content4_text')
        }),
        ('Site Map', {
            'fields': ('show_in_sitemap', 'sitemap_title',)
        }),)


class ContactPageAdmin(DesignOfficePageAdmin):
    form = ContactPageForm
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'headline', 'meta_keywords',
                       'meta_description', 'category', 'active')
        }),
        ('Icons', {
            'fields': ('refresh_image_default', 'refresh_image_hover')
        }),
        ('Main Content', {
            'fields': ('content1_title', 'content1_text', 'content1_image',
                       'content2_title', 'content2_text')
        }),
        ('Site Map', {
            'fields': ('show_in_sitemap', 'sitemap_title',)
        }),)


class CategoryAdmin(admin.ModelAdmin):
    readonly_fields = ('slug',)

    def formfield_for_dbfield(self, db_field, **kwargs):
        if 'image' in db_field.name:
            kwargs.pop('request')
            kwargs['widget'] = ImageForeignKeyWidget(
                db_field.rel, admin_site=self.admin_site)
            return db_field.formfield(**kwargs)

        return super(CategoryAdmin, self).formfield_for_dbfield(
            db_field, **kwargs)

admin.site.register(SitePage, SitePageAdmin)
admin.site.register(DesignOfficePage, DesignOfficePageAdmin)
admin.site.register(SelectedClientsPage, SelectedClientsPageAdmin)
admin.site.register(EssentialsPage, EssentialsPageAdmin)
admin.site.register(CeePage, CeePageAdmin)
admin.site.register(ContactPage, ContactPageAdmin)
admin.site.register(Category, CategoryAdmin)
