from django.utils import unittest
from django.test.client import Client
from django.core.urlresolvers import reverse
from golinko.sitepages.models import SitePage, Category, Essential,\
    DesignOfficePage
from golinko.sidebars.models import Testimonial, Award
from golinko.landingpages.models import PassthroughPage
from cms.apps.media.models import Image
from django.conf import settings
from django.core.files import File


class SitePagesTest(unittest.TestCase):
    """
    1. Check that response code is 200
    2. Check context
    """
    def test_site_pages(self):
        client = Client()

        media_image = Image.objects.create(
            filename=File(open(settings.BASE_PATH + "/static/test/1.png")))
        passthrough_page = PassthroughPage.objects.create(
            title='title',
            image=media_image)
        category = Category.objects.create(
            title='About',
            slug='about',
            menu_image_default=media_image,
            menu_image_hover=media_image,
            menu_image_active=media_image,
            background_image=media_image,
            passthrough_page=passthrough_page)
        award = Award.objects.create(
            title='Award title',
            text='text')
        data = dict(
            title='title',
            slug='design-office',
            headline='headline',
            meta_keywords='meta_keywords',
            meta_description='meta_description',
            category=category,
            active='active',
            content1_title='content1_title',
            content1_text='content1_text',
            content2_title='content2_title',
            content2_text='content2_text',
            content3_title='content3_title',
            content3_text='content3_text',
            content4_title='content4_title',
            content4_text='content4_text',
            show_in_sitemap=True,
            sitemap_title='sitemap_title',
        )

        page = DesignOfficePage.objects.create(**data)
        url = reverse('site_page', args=('design-office',))
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['page'].slug, page.slug)

        # Check that list of essentials passed to context
        self.assertEqual(
            response.context['awards'].count(),
            Award.objects.filter(published=True).count())
        self.assertIn(award, response.context['awards'])

    def test_my_client_say_page(self):
        client = Client()

        testimonial1 = Testimonial.objects.create(
            quote='Quote', person='person', company='company')
        response = client.get(reverse('my-client-say'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, Testimonial.objects.filter(published=True).count())

    def test_submenu_items(self):
        client = Client()

        media_image = Image.objects.create(
            filename=File(open(settings.BASE_PATH + "/static/test/1.png")))
        passthrough_page = PassthroughPage.objects.create(
            title='title',
            image=media_image)
        category = Category.objects.create(
            title='category',
            slug='category',
            menu_image_default=media_image,
            menu_image_hover=media_image,
            menu_image_active=media_image,
            background_image=media_image,
            passthrough_page=passthrough_page)
        data = dict(
            title='title',
            slug='brand-design',
            headline='headline',
            meta_keywords='meta_keywords',
            meta_description='meta_description',
            category=category,
            active='active',
            content1_title='content1_title',
            content1_text='content1_text',
            content2_title='content2_title',
            content2_text='content2_text',
            content3_title='content3_title',
            content3_text='content3_text',
            content4_title='content4_title',
            content4_text='content4_text',
            show_in_sitemap=True,
            sitemap_title='sitemap_title',
        )

        page2 = SitePage.objects.create(**data)
        response = client.get('/p/submenu/?slug=brand-design')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['page'].slug, page2.slug)

    def test_main_area(self):
        client = Client()

        media_image = Image.objects.create(
            filename=File(open(settings.BASE_PATH + "/static/test/1.png")))
        passthrough_page = PassthroughPage.objects.create(
            title='title',
            image=media_image)
        category = Category.objects.create(
            title='category',
            slug='category',
            menu_image_default=media_image,
            menu_image_hover=media_image,
            menu_image_active=media_image,
            background_image=media_image,
            passthrough_page=passthrough_page)

        response = client.get(reverse('home'))

        self.assertEqual(response.status_code, 200)
