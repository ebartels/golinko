from django.db import models
from django.core.exceptions import ValidationError

from cms.apps.utils.fields import AutoSlugField
from cms.apps.media.models import RelatedImagesField
from cms.apps import menus

from adminsortable.models import Sortable


class Category(models.Model):
    title = models.CharField(max_length=100)
    slug = AutoSlugField(max_length=50, unique=True,
                         prepopulate_from='title')
    menu_image_default = models.ForeignKey(
        'media.Image', related_name='menu_image_default')
    menu_image_hover = models.ForeignKey(
        'media.Image', related_name='menu_image_hover')
    menu_image_active = models.ForeignKey(
        'media.Image', related_name='menu_image_active')
    background_image = models.ForeignKey(
        'media.Image', related_name='background_image')
    link = models.CharField(
        max_length=255, null=True, blank=True,
        help_text='Link must be such as, for example: <b>/p/sitepage/</b>'
        '- if the link is to sitepage of this site, <b>/l/landingpage/</b>'
        '- if the link is to landingpage of this site,'
        ' or <b>http://site.com</b> - if the link is to another site')
    passthrough_page = models.ForeignKey('landingpages.PassthroughPage')
    show_on_main = models.BooleanField(help_text='Show this menu item on'
                                                 ' the main page')
    show_on_pages = models.BooleanField(
        default=False, help_text='Show this menu item on the other pages')

    class Meta:
        app_label = 'menus'
        verbose_name_plural = "Categories"

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'category', (self.slug,)


class SitePage(models.Model):
    title = models.CharField(max_length=50)
    slug = AutoSlugField(max_length=50, unique=True,
                         prepopulate_from='title')
    headline = models.CharField(max_length=255)
    meta_keywords = models.TextField(blank=True)
    meta_description = models.TextField(blank=True)
    category = models.ForeignKey('menus.Category', verbose_name='Category')
    active = models.BooleanField(default=True)

    content1_title = models.CharField('Title 1', max_length=255)
    content1_text = models.TextField('Text 1')
    content1_image = models.ForeignKey(
        'media.Image', verbose_name='Image 1', related_name='content1_image',
        null=True, blank=True)
    content2_title = models.CharField('Title 2', max_length=255,
                                      null=True, blank=True)
    content2_text = models.TextField('Text 2',
                                     null=True, blank=True)
    link_text = models.CharField(max_length=255, blank=True)

    content3_title = models.CharField('Col 1 Title', max_length=255)
    content3_text = models.TextField('Col 1 Text')
    content3_image = models.ForeignKey(
        'media.Image', verbose_name='Col 1 Image',
        related_name='content3_image', null=True, blank=True)
    content4_title = models.CharField('Col 2 Title', max_length=255)
    content4_text = models.TextField('Col 2 Text')

    show_contact_form = models.BooleanField(default=False,
            help_text="Show a contact form on this page?")

    show_in_sitemap = models.BooleanField(default=True)
    sitemap_title = models.CharField(max_length=255, blank=True)

    images = RelatedImagesField()

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'site_page', (self.slug,)


class DesignOfficePage(models.Model):
    title = models.CharField(max_length=50)
    slug = AutoSlugField(
        max_length=50, unique=True,
        prepopulate_from='title',
        help_text='For correct view this page,'
                  ' slug field should be: <b>design-office</b>')
    headline = models.CharField(max_length=255)
    meta_keywords = models.TextField(blank=True)
    meta_description = models.TextField(blank=True)
    category = models.ForeignKey(
        'menus.Category', verbose_name='Category',
        related_name='design_office_category',
        help_text='For correct view '
                  'this page, category should be: <b>about</b>')
    active = models.BooleanField(default=True)

    menu_image_default = models.ForeignKey(
        'media.Image', related_name='design_office_menu_image_default',
        null=True, blank=True)
    menu_image_hover = models.ForeignKey(
        'media.Image', related_name='design_office_menu_image_hover',
        null=True, blank=True)
    menu_image_active = models.ForeignKey(
        'media.Image', related_name='design_office_menu_image_active',
        null=True, blank=True)

    content1_title = models.CharField('Title 1', max_length=255,
                                      null=True, blank=True)
    content1_text = models.TextField('Text 1')
    content1_image = models.ForeignKey(
        'media.Image', verbose_name='Image 1',
        related_name='design_office_content1_image', null=True, blank=True)

    content2_title = models.CharField('Title 2', max_length=255)
    content2_text = models.TextField('Text 2')

    content3_title = models.CharField('Col 1 Title', max_length=255)
    content3_text = models.TextField('Col 1 Text')
    content3_image = models.ForeignKey(
        'media.Image', verbose_name='Col 1 Image',
        related_name='design_office_content3_image', null=True, blank=True)

    content4_title = models.CharField('Col 2 Title', max_length=255)
    content4_text = models.TextField('Col 2 Text')

    show_contact_form = models.BooleanField(default=False,
            help_text="Show a contact form on this page?")

    show_in_sitemap = models.BooleanField(default=True)
    sitemap_title = models.CharField(max_length=255, blank=True)

    class Meta:
        verbose_name = "Design Office page"
        verbose_name_plural = "Design Office pages"

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'site_page', (self.slug,)


class SelectedClientsPage(models.Model):
    title = models.CharField(max_length=50)
    slug = AutoSlugField(
        max_length=50, unique=True,
        prepopulate_from='title',
        help_text='For correct view this page,'
                  ' slug field should be: <b>selected-clients</b>')
    headline = models.CharField(max_length=255)
    meta_keywords = models.TextField(blank=True)
    meta_description = models.TextField(blank=True)
    category = models.ForeignKey(
        'menus.Category', verbose_name='Category',
        related_name='selected_clients_category',
        help_text='For correct '
                  'view this page, category should be: <b>about</b>')
    active = models.BooleanField(default=True)

    menu_image_default = models.ForeignKey(
        'media.Image', related_name='selected_clients_menu_image_default',
        null=True, blank=True)
    menu_image_hover = models.ForeignKey(
        'media.Image', related_name='selected_clients_menu_image_hover',
        null=True, blank=True)
    menu_image_active = models.ForeignKey(
        'media.Image', related_name='selected_clients_menu_image_active',
        null=True, blank=True)

    content1_title = models.CharField('Title 1', max_length=255)
    content1_text = models.TextField('Text 1')
    content1_image = models.ForeignKey(
        'media.Image', verbose_name='Image 1',
        related_name='selected_clients_content1_image', null=True, blank=True)

    col1_text = models.TextField('Col 1')
    col2_text = models.TextField('Col 2')

    content3_title = models.CharField('Col 1 Title', max_length=255)
    content3_text = models.TextField('Col 1 Text')
    content3_image = models.ForeignKey(
        'media.Image', verbose_name='Col 1 Image',
        related_name='selected_clients_content3_image', null=True, blank=True)

    content4_title = models.CharField('Col 2 Title', max_length=255)
    content4_text = models.TextField('Col 2 Text')

    show_contact_form = models.BooleanField(default=False,
            help_text="Show a contact form on this page?")

    show_in_sitemap = models.BooleanField(default=True)
    sitemap_title = models.CharField(max_length=255, blank=True)

    class Meta:
        verbose_name = "Selected Clients page"
        verbose_name_plural = "Selected Clients pages"

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'site_page', (self.slug,)


class EssentialsPage(models.Model):
    title = models.CharField(max_length=50)
    slug = AutoSlugField(
        max_length=50, unique=True,
        prepopulate_from='title',
        help_text='For correct view this page,'
                  ' slug field should be: <b>12-essentials</b>')
    headline = models.CharField(max_length=255)
    meta_keywords = models.TextField(blank=True)
    meta_description = models.TextField(blank=True)
    category = models.ForeignKey(
        'menus.Category', verbose_name='Category',
        related_name='essentials_category',
        help_text='For correct view this '
                  'page, category should be: <b>about</b>')
    active = models.BooleanField(default=True)

    menu_image_default = models.ForeignKey(
        'media.Image', related_name='essentials_menu_image_default',
        null=True, blank=True)
    menu_image_hover = models.ForeignKey(
        'media.Image', related_name='essentials_menu_image_hover',
        null=True, blank=True)
    menu_image_active = models.ForeignKey(
        'media.Image', related_name='essentials_menu_image_active',
        null=True, blank=True)

    content1_title = models.CharField('Title 1', max_length=255)

    essential1_title = models.CharField('Essential Title 1', max_length=255)
    essential1_text = models.TextField('Essential Text 1')
    essential2_title = models.CharField('Essential Title 2', max_length=255)
    essential2_text = models.TextField('Essential Text 2')
    essential3_title = models.CharField('Essential Title 3', max_length=255)
    essential3_text = models.TextField('Essential Text 3')
    essential4_title = models.CharField('Essential Title 4', max_length=255)
    essential4_text = models.TextField('Essential Text 4')
    essential5_title = models.CharField('Essential Title 5', max_length=255)
    essential5_text = models.TextField('Essential Text 5')
    essential6_title = models.CharField('Essential Title 6', max_length=255)
    essential6_text = models.TextField('Essential Text 6')
    essential7_title = models.CharField('Essential Title 7', max_length=255)
    essential7_text = models.TextField('Essential Text 7')
    essential8_title = models.CharField('Essential Title 8', max_length=255)
    essential8_text = models.TextField('Essential Text 8')
    essential9_title = models.CharField('Essential Title 9', max_length=255)
    essential9_text = models.TextField('Essential Text 9')
    essential10_title = models.CharField('Essential Title 10', max_length=255)
    essential10_text = models.TextField('Essential Text 10')
    essential11_title = models.CharField('Essential Title 11', max_length=255)
    essential11_text = models.TextField('Essential Text 11')
    essential12_title = models.CharField('Essential Title 12', max_length=255)
    essential12_text = models.TextField('Essential Text 12')

    content3_title = models.CharField('Col 1 Title', max_length=255)
    content3_text = models.TextField('Col 1 Text')
    content3_image = models.ForeignKey(
        'media.Image', verbose_name='Col 1 Image',
        related_name='essentials_content3_image', null=True, blank=True)

    content4_title = models.CharField('Col 2 Title', max_length=255)
    content4_text = models.TextField('Col 2 Text')

    show_contact_form = models.BooleanField(default=False,
            help_text="Show a contact form on this page?")

    show_in_sitemap = models.BooleanField(default=True)
    sitemap_title = models.CharField(max_length=255, blank=True)

    class Meta:
        verbose_name = "12 Essentials page"
        verbose_name_plural = "12 Essentials pages"

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'site_page', (self.slug,)


class CeePage(models.Model):
    title = models.CharField(max_length=50)
    slug = AutoSlugField(
        max_length=50, unique=True,
        prepopulate_from='title',
        help_text='For correct view this page,'
        ' slug field should be: <b>creativity-experience-excellence</b>')
    headline = models.CharField(max_length=255)
    meta_keywords = models.TextField(blank=True)
    meta_description = models.TextField(blank=True)
    category = models.ForeignKey(
        'menus.Category', verbose_name='Category',
        related_name='cee_category',
        help_text='For correct view this '
                  'page, category should be: <b>about</b>')
    active = models.BooleanField(default=True)

    menu_image_default = models.ForeignKey(
        'media.Image', related_name='cee_menu_image_default',
        null=True, blank=True)
    menu_image_hover = models.ForeignKey(
        'media.Image', related_name='cee_menu_image_hover',
        null=True, blank=True)
    menu_image_active = models.ForeignKey(
        'media.Image', related_name='cee_menu_image_active',
        null=True, blank=True)

    content1_title = models.CharField('Title 1', max_length=255)
    content1_text = models.TextField('Text 1')
    content1_image = models.ForeignKey(
        'media.Image', verbose_name='Image 1',
        related_name='cee_content1_image', null=True, blank=True)

    content3_title = models.CharField('Col 1 Title', max_length=255)
    content3_text = models.TextField('Col 1 Text')
    content3_image = models.ForeignKey(
        'media.Image', verbose_name='Col 1 Image',
        related_name='cee_content3_image', null=True, blank=True)

    content4_title = models.CharField('Col 2 Title', max_length=255)
    content4_text = models.TextField('Col 2 Text')

    show_contact_form = models.BooleanField(default=False,
            help_text="Show a contact form on this page?")

    show_in_sitemap = models.BooleanField(default=True)
    sitemap_title = models.CharField(max_length=255, blank=True)

    class Meta:
        verbose_name = "CE&E page"
        verbose_name_plural = "CE&E pages"

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'site_page', (self.slug,)


class ContactPage(models.Model):
    title = models.CharField(max_length=50)
    slug = AutoSlugField(
        max_length=50, unique=True,
        prepopulate_from='title',
        help_text='For correct view this page,'
                  ' slug field should be: <b>contacts</b>')
    headline = models.CharField(max_length=255)
    meta_keywords = models.TextField(blank=True)
    meta_description = models.TextField(blank=True)
    category = models.ForeignKey(
        'menus.Category', verbose_name='Category',
        related_name='contact_category',
        help_text='For correct view this '
                  'page, category should be: <b>contact</b>')
    active = models.BooleanField(default=True)

    refresh_image_default = models.ForeignKey(
        'media.Image', related_name='refresh_image_default',
        null=True, blank=True)
    refresh_image_hover = models.ForeignKey(
        'media.Image', related_name='refresh_image_hover',
        null=True, blank=True)

    content1_title = models.CharField('Title 1', max_length=255)
    content1_text = models.TextField('Text 1')
    content1_image = models.ForeignKey(
        'media.Image', verbose_name='Image 1',
        related_name='contact_content1_image', null=True, blank=True)

    content2_title = models.CharField('Title 2', max_length=255)
    content2_text = models.TextField('Text 2')

    show_in_sitemap = models.BooleanField(default=True)
    sitemap_title = models.CharField(max_length=255, blank=True)

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'site_page', (self.slug,)


class Essential(Sortable):
    class Meta(Sortable.Meta):
        pass

    title = models.CharField(max_length=100)
    text = models.TextField()
    published = models.BooleanField(default=True)

    def __unicode__(self):
        return self.title


menus.register_model(Category)
menus.register_model(SitePage)
menus.register_model(DesignOfficePage)
menus.register_model(SelectedClientsPage)
menus.register_model(EssentialsPage)
menus.register_model(CeePage)
menus.register_model(ContactPage)
