from django.conf.urls.defaults import url, patterns

urlpatterns = patterns('golinko.sitepages.views',
    url(r'^passthrough-text/(?P<slug>[\w_-]+)/$', 'passthrough_text', name='passthrough_text'),
    url(r'^my_client_say/$', 'my_client_say', name='my-client-say'),
    url(r'^submenu/$', 'submenu_items', name='submenu-items'),
    url(r'^(?P<slug>[\w_-]+)/$', 'site_page', name='site_page'),
)
