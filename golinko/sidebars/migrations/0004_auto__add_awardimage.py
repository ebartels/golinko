# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AwardImage'
        db.create_table('sidebars_awardimage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('award_title_image', self.gf('django.db.models.fields.related.ForeignKey')(related_name='award_title_image', to=orm['media.Image'])),
            ('category', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['menus.Category'], unique=True)),
        ))
        db.send_create_signal('sidebars', ['AwardImage'])


    def backwards(self, orm):
        # Deleting model 'AwardImage'
        db.delete_table('sidebars_awardimage')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'landingpages.passthroughpage': {
            'Meta': {'object_name': 'PassthroughPage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Image']"}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'media.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['media.MediaItem']},
            'alt_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'filename': ('cms.apps.media.fields.ImageField', [], {'max_length': '100'}),
            'height': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'mediaitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['media.MediaItem']", 'unique': 'True', 'primary_key': 'True'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'media.mediaitem': {
            'Meta': {'object_name': 'MediaItem'},
            'caption': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polymorphic_media.mediaitem_set'", 'null': 'True', 'to': "orm['contenttypes.ContentType']"}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'uuid': ('cms.apps.utils.fields.UUIDB64Field', [], {'default': "u'RTsRZh9MSCC2mZdoRf0Jrg'", 'unique': 'True', 'max_length': '22'})
        },
        'menus.category': {
            'Meta': {'object_name': 'Category'},
            'background_image': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'background_image'", 'to': "orm['media.Image']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'menu_image_active': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'menu_image_active'", 'to': "orm['media.Image']"}),
            'menu_image_default': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'menu_image_default'", 'to': "orm['media.Image']"}),
            'menu_image_hover': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'menu_image_hover'", 'to': "orm['media.Image']"}),
            'passthrough_page': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['landingpages.PassthroughPage']"}),
            'show_on_main': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'show_on_pages': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sidebars.award': {
            'Meta': {'object_name': 'Award'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1', 'db_index': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sidebars.awardimage': {
            'Meta': {'object_name': 'AwardImage'},
            'award_title_image': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'award_title_image'", 'to': "orm['media.Image']"}),
            'category': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['menus.Category']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'sidebars.testimonial': {
            'Meta': {'ordering': "['order']", 'object_name': 'Testimonial'},
            'company': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1', 'db_index': 'True'}),
            'person': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'quote': ('django.db.models.fields.TextField', [], {})
        },
        'taggit.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'})
        },
        'taggit.taggeditem': {
            'Meta': {'object_name': 'TaggedItem'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taggit_taggeditem_tagged_items'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taggit_taggeditem_items'", 'to': "orm['taggit.Tag']"})
        }
    }

    complete_apps = ['sidebars']