 (function($){

     $.testimonial_num = 0
     $.init_testimonials = function(my_url){
        $("#my_client_say_button").live('click', function(){
            $.get(my_url, { testimonial_num: $.testimonial_num},
                function(data){
                    $("#my_client_say_container").html(data.html);
                    $.testimonial_num = data.next_num
                });
        })
    };

})(jQuery);
