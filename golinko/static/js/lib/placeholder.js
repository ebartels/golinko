/*global Modernizr: true*/
/*
 * A input[placeholder] polyfill
 */
(function($) {
    "use strict";

    /* Emulate input[placeholder] */
    $.fn.placeholder = function() {
        if (Modernizr.input.placeholder) {
            return this;
        }
        var inputs = this,
            form = inputs.closest('form');

        inputs.each(function() {
            var input = $(this),
                placeholder = input.attr('placeholder');

            input.attr('placeholder', '');

            var show_placeholder = function() {
                if (input.val() === '') {
                    input.val(placeholder).addClass('placeholder');
                }
            };
            var hide_placeholder = function() {
                if (input.val() === placeholder) {
                    input.val('').removeClass('placeholder');
                }
            };
            input.focus(hide_placeholder).blur(show_placeholder);
            show_placeholder();
        });

        form.on('submit', function(e) {
            inputs.filter('.placeholder').each(function() {
                $(this).val('');
            });
        });

        return inputs;
    };
})(jQuery);
