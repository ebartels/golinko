"""
Project settings file

Any custom settings will be picked up from config.local.  Settings below are
also configurable by setting their respective environment variables.
"""
from os import environ as env
from django.core.exceptions import ImproperlyConfigured

# import production or dev settings
ENVIRONMENT = env.get('DJANGO_ENV', 'dev')
if ENVIRONMENT == 'dev':
    from config.dev import *
else:
    from config.production import *

# import any local settings
for config in ('local',):
    config_path = os.path.join(BASE_PATH, 'config', '{0}.py'.format(config))
    try:
        execfile(os.path.abspath(config_path))
    except IOError:
        pass

DEBUG = env.get('DEBUG', DEBUG)
TEMPLATE_DEBUG = env.get('TEMPLATE_DEBUG', TEMPLATE_DEBUG)

# settings overrides from environment variables
SECRET_KEY = env.get('SECRET_KEY', locals().get('SECRET_KEY'))
if not SECRET_KEY:
    raise ImproperlyConfigured("The SECRET_KEY setting is missing. "
        "You can set a SECRET_KEY environment variable, or create "
        "conf/local.py settings file and add it there.")

INTERNAL_IPS = env.get('INTERNAL_IPS', locals().get('INTERNAL_IPS', tuple()))

# database setting uses url
# see https://github.com/kennethreitz/dj-database-url
try:
    import dj_database_url
    try:
        DATABASES
    except NameError:
        DATABASES = {
            'default': dj_database_url.config(
                        default='sqlite:///%s/db.sqlite' % BASE_PATH)
        }
except ImportError:
    pass

# AWS credientials
AWS_ACCESS_KEY_ID = env.get('AWS_ACCESS_KEY_ID', locals().get('AWS_ACCESS_KEY_ID'))
AWS_SECRET_ACCESS_KEY = env.get('AWS_SECRET_ACCESS_KEY', locals().get('AWS_SECRET_ACCESS_KEY'))
