var ImageGallery;

(function($) {
    "use strict";

    /*
     * Image Gallery / Slideshow widget
     */
    ImageGallery = function() {
        return this.initialize.apply(this, arguments);
    };

    ImageGallery.prototype = {
        defaults: {
            el: '#image-gallery',
            interval: 4000
        },

        initialize: function(opt) {
            _.bindAll(this);
            this.o = $.extend(this.defaults, opt);
            this.el = $(this.o.el);
            this.images = this.el.find('>img');
            this.nav = $(this.el).find('.nav');
            this.caption = $(this.el).find('.image-caption');
            this._cycling = true;

            this.render();
            this.show(0);
            this.start();

            this.navs.on('click', this.onNavClick);
            this.images.on('click', this.onClick);
            this.images.on('mouseenter', this.onMouseEnter);
            this.images.on('mouseleave', this.onMouseLeave);
        },

        render: function() {
            var self = this;
            this.images.each(function() {
                self.nav.append('<span class="item"><span></span></span>');
            });
            this.navs = this.nav.find('.item');
        },

        index: function(i) {
            return (i + this.images.length) % this.images.length;
        },

        show: function(i) {
            i = this.index(i);
            this.images.filter('.active').removeClass('active');
            this.images.eq(i).addClass('active');
            this.navs.filter('.active').removeClass('active');
            this.navs.eq(i).addClass('active');
            this.caption.filter('.active').removeClass('active');
            this.caption.eq(i).addClass('active');
        },

        advance: function() {
            var last = this.images.filter('.active'),
                index = this.index(last.index() +1);
            this.show(index);
        },

        start: function() {
            this.pause();
            if (this._cycling) {
                this.interval = setInterval(this.advance, this.o.interval);
            }
        },

        pause: function() {
            if (this.interval) {
                clearInterval(this.interval);
            }
        },

        stop: function() {
            this._cycling = false;
            this.pause();
        },

        onNavClick: function(e) {
            this.stop();
            var i = $(e.currentTarget).index();
            this.show(i);
        },

        onClick:function(e) {
            this.advance();
        },

        onMouseEnter: function(e) {
            this.pause();
        },

        onMouseLeave: function(e) {
            this.start();
        }
    };
})(jQuery);
