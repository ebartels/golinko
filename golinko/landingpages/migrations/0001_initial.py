# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'LandingPage'
        db.create_table('landingpages_landingpage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('slug', self.gf('cms.apps.utils.fields.AutoSlugField')(prepopulate_from='title', unique=True, max_length=50)),
            ('headline', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('meta_keywords', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('meta_description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('content1_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('content1_text', self.gf('django.db.models.fields.TextField')()),
            ('content2_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('content2_text', self.gf('django.db.models.fields.TextField')()),
            ('link_text', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('content3_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('content3_text', self.gf('django.db.models.fields.TextField')()),
            ('content3_image', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Image'], null=True, blank=True)),
            ('content4_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('content4_text', self.gf('django.db.models.fields.TextField')()),
            ('column_layout', self.gf('django.db.models.fields.CharField')(default='3col', max_length=10)),
            ('column1_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('column1_text', self.gf('django.db.models.fields.TextField')()),
            ('column2_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('column2_text', self.gf('django.db.models.fields.TextField')()),
            ('column3_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('column3_text', self.gf('django.db.models.fields.TextField')()),
            ('show_in_sitemap', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('sitemap_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('related_images', self.gf('cms.apps.media.fields.related.MediaGenericRelation')(to=orm['media.MediaSubRelation'])),
        ))
        db.send_create_signal('landingpages', ['LandingPage'])

        # Adding model 'PassthroughPage'
        db.create_table('landingpages_passthroughpage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('meta_keywords', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('meta_description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('image', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Image'])),
        ))
        db.send_create_signal('landingpages', ['PassthroughPage'])

        # Adding model 'PassthroughOverlay'
        db.create_table('landingpages_passthroughoverlay', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('passthrough_page', self.gf('django.db.models.fields.related.ForeignKey')(related_name='overlays', to=orm['landingpages.PassthroughPage'])),
            ('image', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['media.Image'])),
            ('width', self.gf('django.db.models.fields.PositiveIntegerField')(default=100)),
            ('height', self.gf('django.db.models.fields.PositiveIntegerField')(default=100)),
            ('top', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('left', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal('landingpages', ['PassthroughOverlay'])


    def backwards(self, orm):
        # Deleting model 'LandingPage'
        db.delete_table('landingpages_landingpage')

        # Deleting model 'PassthroughPage'
        db.delete_table('landingpages_passthroughpage')

        # Deleting model 'PassthroughOverlay'
        db.delete_table('landingpages_passthroughoverlay')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'landingpages.landingpage': {
            'Meta': {'object_name': 'LandingPage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'column1_text': ('django.db.models.fields.TextField', [], {}),
            'column1_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'column2_text': ('django.db.models.fields.TextField', [], {}),
            'column2_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'column3_text': ('django.db.models.fields.TextField', [], {}),
            'column3_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'column_layout': ('django.db.models.fields.CharField', [], {'default': "'3col'", 'max_length': '10'}),
            'content1_text': ('django.db.models.fields.TextField', [], {}),
            'content1_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content2_text': ('django.db.models.fields.TextField', [], {}),
            'content2_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content3_image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Image']", 'null': 'True', 'blank': 'True'}),
            'content3_text': ('django.db.models.fields.TextField', [], {}),
            'content3_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content4_text': ('django.db.models.fields.TextField', [], {}),
            'content4_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'related_images': ('cms.apps.media.fields.related.MediaGenericRelation', [], {'to': "orm['media.MediaSubRelation']"}),
            'show_in_sitemap': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sitemap_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'landingpages.passthroughoverlay': {
            'Meta': {'object_name': 'PassthroughOverlay'},
            'height': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Image']"}),
            'left': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'passthrough_page': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'overlays'", 'to': "orm['landingpages.PassthroughPage']"}),
            'top': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'})
        },
        'landingpages.passthroughpage': {
            'Meta': {'object_name': 'PassthroughPage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Image']"}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'media.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['media.MediaItem']},
            'alt_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'filename': ('cms.apps.media.fields.ImageField', [], {'max_length': '100'}),
            'height': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'mediaitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['media.MediaItem']", 'unique': 'True', 'primary_key': 'True'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'media.mediaitem': {
            'Meta': {'object_name': 'MediaItem'},
            'caption': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polymorphic_media.mediaitem_set'", 'null': 'True', 'to': "orm['contenttypes.ContentType']"}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'uuid': ('cms.apps.utils.fields.UUIDB64Field', [], {'default': "u'XV8IOv-dQwO_SnsedZEo1Q'", 'unique': 'True', 'max_length': '22'})
        },
        'media.mediarelation': {
            'Meta': {'ordering': "('content_type', 'sort')", 'unique_together': "(('item', 'object_id', 'content_type'),)", 'object_name': 'MediaRelation'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'mediarelation_set'", 'to': "orm['media.MediaItem']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'sort': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'})
        },
        'media.mediasubrelation': {
            'Meta': {'ordering': "('content_type', 'sort')", 'object_name': 'MediaSubRelation', 'db_table': "'media_mediarelation'", '_ormbases': ['media.MediaRelation'], 'proxy': 'True'}
        },
        'taggit.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'})
        },
        'taggit.taggeditem': {
            'Meta': {'object_name': 'TaggedItem'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taggit_taggeditem_tagged_items'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taggit_taggeditem_items'", 'to': "orm['taggit.Tag']"})
        }
    }

    complete_apps = ['landingpages']