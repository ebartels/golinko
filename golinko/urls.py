from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

# import livesettings config & admin configuration
from golinko.config import livesettings_config, admin as acadmin


urlpatterns = patterns('',
    # Homepage
    url(r'^$', 'golinko.sitepages.views.index', name='home'),
    url(r'site-map/$', 'golinko.landingpages.views.site_map', name='site_map'),

    # Categories
    url(r'^c/(?P<slug>[\w_-]+)/$', 'golinko.sitepages.views.category', name='category'),

    # Pass-through page to old site
    url(r'^to-portfolio/', 'golinko.landingpages.views.passthrough', name='pass-through'),

    # Apps
    url(r'^l/', include('golinko.landingpages.urls')),
    url(r'^media/', include('cms.apps.media.urls')),
    url(r'^p/', include('golinko.sitepages.urls')),

    # Admin
    url(r'^admin/settings/', include('livesettings.urls')),
    url(r'^admin/media/', include('cms.apps.media.admin.urls')),
    url(r'^admin/', include('cms.apps.cmsadmin.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

# media file serving for development server
urlpatterns += static(settings.MEDIA_URL,
                      document_root=settings.MEDIA_ROOT,
                      show_indexes=True)
